<?php
/**
 * Created by PhpStorm.
 * User: francois
 * Date: 10/10/15
 * Time: 15:34
 */

require_once __DIR__.'/../vendor/autoload.php';

use Knp\Provider\ConsoleServiceProvider;
use Silex\Provider\DoctrineServiceProvider;

$app = new Silex\Application();
$app->register(
    new ConsoleServiceProvider(),
    [
        'console.name'              => 'IadvizeConsole',
        'console.version'           => '0.1.0',
        'console.project_directory' => __DIR__."/..",
    ]
);
$app->register(
    new DoctrineServiceProvider(),
    [
        'db.options' => [
            'driver'   => 'pdo_mysql',
            'host'     => '127.0.0.1',
            'dbname'   => 'vdm_database',
            'user'     => 'iadvize',
            'password' => 'iadvize',
            'charset'  => 'utf8mb4',
        ],
    ]
);
