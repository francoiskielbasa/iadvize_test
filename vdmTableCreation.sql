drop table vdm;
create table vdm (
    id INT unsigned primary key,
    content varchar(300) not null,
    author varchar(255) not null,
    date datetime not null
    );
