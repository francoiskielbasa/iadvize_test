<?php
/**
 * Created by PhpStorm.
 * User: francois
 * Date: 09/10/15
 * Time: 22:52
 */

namespace Francois\Iadvize;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Knp\Command\Command;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class VdmCommand, script to load and store content from http://www.viedemerde.fr
 *
 * @extends Knp\Command\Command
 * @package Francois\Iadvize
 */
class VdmCommand extends Command
{
    /**
     * @var \Doctrine\DBAL\Connection $database
     */
    private $database;

    protected function configure()
    {
        $this
            ->setName('vdm:load')
            ->setDescription('Load and save first 200 records on Vie De Merde')
            ->addArgument(
                'number',
                InputArgument::OPTIONAL,
                'How many Vdm do you want to load ?'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->loadDatabase();
        $output->writeln('Database reset...');
        $this->deleteAllVdms();
        $output->writeln('VDMs recording...');
        $vdmNumber = 0;
        $pageNumber = 0;
        $numberOfVdm = $input->getArgument('number');
        if (!$numberOfVdm) {
            $numberOfVdm = 200;
        }
        while ($vdmNumber < $numberOfVdm) {
            $document = $this->loadVdmPage($pageNumber);
            $crawler = new Crawler($document);
            $filter = $crawler->filter('.article');
            foreach ($filter as $value) {
                $crawlerValue = new Crawler($value);
                $content = $crawlerValue->filter('p')->text();
                $dateAuthor = $crawlerValue->filter('.date .right_part p+p')->text();
                $liste = [];
                preg_match('/(\d{2}\/\d{2}\/\d{4})( à )(\d{2}:\d{2})(.*par )(.*)( .*)/', $dateAuthor, $liste);
                $date = $liste[1];
                $time = $liste[3];
                $author = $liste[5];
                $id = $vdmNumber + 1;
                $this->recordVdm($id, $content, $date, $time, $author);
                $output->writeln('Record VDM #'.$id);
                $vdmNumber++;
                if ($vdmNumber >= $numberOfVdm) {
                    break;
                }
            }
            $pageNumber++;
        }
    }

    /**
     * Load html content from http://www.viedemerde.fr
     *
     * @param int $pageNb
     * @return string
     */
    private function loadVdmPage($pageNb = 0)
    {
        if ($pageNb == 0) {
            $query = null;
        } else {
            $query = '/?page='.$pageNb;
        }
        $document = file_get_contents('http://www.viedemerde.fr'.$query);

        return $document;
    }

    /**
     * Record VDM in the database
     *
     * @param int $vdmId
     * @param string $content
     * @param string $date
     * @param string $time
     * @param string $author
     */
    private function recordVdm($vdmId, $content, $date, $time, $author)
    {
        $dateTime = $this->formatDate($date, $time);

        $this->database->insert(
            'vdm',
            ['id' => $vdmId, 'content' => $content, 'author' => $author, 'date' => $dateTime]
        );
    }

    /**
     * Format date and time in SQL datetime format
     *
     * @param string $date
     * @param string $time
     * @return string
     */
    private function formatDate($date, $time)
    {
        $date = str_replace('/', '-', $date);
        $date = date('Y-m-d', strtotime($date));

        return ($date.' '.$time.':00');
    }

    /**
     * Delete all records in the database
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    private function deleteAllVdms()
    {
        $this->database->executeUpdate('DELETE FROM vdm');
    }

    /**
     * Load database from silex application
     */
    private function loadDatabase()
    {
        $silex = $this->getSilexApplication();
        $this->database = $silex['db'];
    }
}
